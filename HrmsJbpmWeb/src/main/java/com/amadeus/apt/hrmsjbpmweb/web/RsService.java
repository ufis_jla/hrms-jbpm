package com.amadeus.apt.hrmsjbpmweb.web;

import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.ext.Provider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amadeus.apt.hrmsjbpmcore.process.DroolsFlow;

@Stateless
@Path("/")
@Provider
public class RsService {
	
	public static Logger LOGGER = LoggerFactory.getLogger(RsService.class);
	
	@GET
	@Path("/heartBeat")
	@Produces(value = {MediaType.TEXT_PLAIN})
	public String heartBeat() {
		
		LOGGER.debug("heartBeat >>> ");
		DroolsFlow.main(null);
		LOGGER.debug("heartBeat <<< ");
		return "heart is beating";
	}
	
}
