package com.amadeus.apt.hrmsjbpmcore.process;

import java.util.HashMap;
import java.util.Map;

import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;

import com.amadeus.apt.hrmsjbpmcore.dataobject.Staff;

public class DroolsFlow {

	public static void main(String[] args) {
		try {
            // load up the knowledge base
	        KieServices ks = KieServices.Factory.get();
    	    KieContainer kContainer = ks.getKieClasspathContainer();
        	KieSession kSession = kContainer.newKieSession("ksession-process");
        	
        	//Prepare Object
        	Staff staff = prepareStaffObject();
        	Map<String, Object> params = new HashMap<String, Object>();        	 
            params.put("staff", staff);
        	
            // start a new process instance
            kSession.startProcess("staffJBPM", params);
            kSession.fireAllRules();
        } catch (Throwable t) {
            t.printStackTrace();
        }
	}
	
	private static Staff prepareStaffObject(){
		Staff newStaff = new Staff();
		newStaff.setName("Moe moe");
		newStaff.setTotalWorkingHours(5);
		return newStaff;
	}

}
