package com.amadeus.apt.hrmsjbpmcore.dataobject;

public class Staff {

	private String name;
	private int totalWorkingHours;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getTotalWorkingHours() {
		return totalWorkingHours;
	}

	public void setTotalWorkingHours(int totalWorkingHours) {
		this.totalWorkingHours = totalWorkingHours;
	}
	
	public String toString(){
		return "name = "+name+","+"totalWorkingHours = "+totalWorkingHours;
	}

}
